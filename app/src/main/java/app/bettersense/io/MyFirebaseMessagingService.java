package app.bettersense.io;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;

public class MyFirebaseMessagingService extends FirebaseMessagingService
{
    private static final String TAG = "MyFirebaseMessagingService";
    @Override
    public void onNewToken(String token) {
        Intent intent = new Intent(JSCommandDispatcher.CMD_ON_DEVICE_PUSH_TOKEN_UPDATE);
        intent.putExtra("pushToken", token);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
