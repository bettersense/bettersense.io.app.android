package app.bettersense.io;

import android.webkit.ValueCallback;
import android.webkit.WebView;

import org.json.JSONObject;

public class JSCommandDispatcher implements ValueCallback<String>
{
    private static final String TAG = "JSCommandDispatcher";
    public static final String CMD_ON_BACK_PRESSED              = "onBackPressed";
    public static final String CMD_ON_DEVICE_SIGN_IN_COMPLETE   = "onDeviceSignInComplete";
    public static final String CMD_ON_DEVICE_SIGN_IN_COMPLETE2  = "onDeviceSignInComplete2";
    public static final String CMD_ON_DEVICE_PUSH_TOKEN_UPDATE  = "onDevicePushTokenUpdate";

    private ValueCallback<String> mListener;
    public JSCommandDispatcher(WebView view, String command, ValueCallback<String> listener)
    {
        String js = String.format("onDeviceCommand(\"%s\", \"%s\")", command, null);
        mListener = listener;

        BLog.d(TAG, js);
        view.evaluateJavascript(js, this);
    }
    public JSCommandDispatcher(WebView view, String command, String data, ValueCallback<String> listener)
    {
        String js = String.format("onDeviceCommand(\"%s\", \"%s\")", command, data);
        mListener = listener;

        BLog.d(TAG, js);
        view.evaluateJavascript(js, this);
    }
    public JSCommandDispatcher(WebView view, String command, JSONObject json, ValueCallback<String> listener)
    {
        String js = String.format("onDeviceCommand(\"%s\", ", command);
        mListener = listener;

        BLog.d(TAG, js);
        view.evaluateJavascript(js + json + ")", this);
    }
    public JSCommandDispatcher(WebView view, String bridge, String command, String data, ValueCallback<String> listener)
    {
        String js = String.format("%s(\"%s\", \"%s\")", bridge, command, data);
        mListener = listener;

        BLog.d(TAG, js);
        view.evaluateJavascript(js, this);
    }
    public JSCommandDispatcher(WebView view, String bridge, String command, JSONObject json, ValueCallback<String> listener)
    {
        String js = String.format("%s(\"%s\", ", bridge, command);
        mListener = listener;

        BLog.d(TAG, js);
        view.evaluateJavascript(js + json + ")", this);
    }
    ///////////////////////////////////////////////////////////////
    //
    @Override
    public void onReceiveValue(String value) {
        BLog.d(TAG, "onReceiveValue", value);
        if ( null!=mListener )
            mListener.onReceiveValue(value);
    }
}
