package app.bettersense.io;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final String TAG     = "bettersense.io.android";
    private static final String UA      = "bettersense.io.android";
    private static final String HOST    = "AndroidHook";
    private static final int RC_SIGN_IN = 2896;

    private WebView mWebView;
    private View mLoader;
    private WebAppInterface mWebAppInstance;

    private static final String TARGET_API = "http://apitest.bettersense.io:8000/api/v1/auth/oauth/google";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView    = findViewById(R.id.webview);
        mLoader     = findViewById(R.id.loader);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setUserAgentString(UA);

        mWebView.setWebViewClient(new Client());
        mWebView.setWebChromeClient(new ChromeClient());

        mWebAppInstance = new WebAppInterface(this);
        mWebView.addJavascriptInterface(mWebAppInstance, HOST);
        mWebView.loadUrl("https://www.bettersense.io/demo2");
    }
    @Override
    protected void onStart()
    {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter(JSCommandDispatcher.CMD_ON_DEVICE_PUSH_TOKEN_UPDATE)
        );
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    @Override
    public void onBackPressed()
    {
        new JSCommandDispatcher(
                mWebView,
                JSCommandDispatcher.CMD_ON_BACK_PRESSED,
                new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        BLog.d(TAG, "onBackPressed", "onReceiveValue", value);
                        if ( value.contains("false"))
                        {
                            MainActivity.this.finish();
                        }
                    }
                });
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try
        {
            final GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            mWebAppInstance.setSignInToken(account.getIdToken());
            mWebView.post(new Runnable() {
                @Override
                public void run() {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("provider", "google");
                        json.put("idp_access_token", account.getIdToken());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new JSCommandDispatcher(
                            mWebView,
                            JSCommandDispatcher.CMD_ON_DEVICE_SIGN_IN_COMPLETE,
                            json,
                            new ValueCallback<String>() {
                                @Override
                                public void onReceiveValue(String value) {
                                    BLog.d(TAG, "setSignInToken", value);
                                }
                            }
                    );
                }
            });
            BLog.d(TAG, "handleSignInResult", completedTask.isSuccessful());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            BLog.d(TAG, "handleSignInResult", "err", e.getStatusCode(), e.getMessage(), e.toString());
            //updateUI(null);
        }
    }
    /*
    private void postAuthRequest()
    {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject json = new JSONObject();
        try {
            json.put("provider","google");
            json.put("idp_access_token", mWebAppInstance.getSignInToken());
            json.put("auto_create",true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Log.w(TAG, "postAuthRequest=" + json.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, TARGET_API, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.w(TAG, "onResponse=" + response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.w(TAG, "onErrorResponse="+error.toString()+error.getMessage() );
            }
        });

        queue.add(jsonObjectRequest);
    }
    */
    public class WebAppInterface {
        // private static final String TAG = "WebAppInterface";
        private static final String CMD_ON_CLICKED_SIGN_IN = "onClickedSignIn";
        private static final String CMD_GET_SIGN_IN_TOKEN = "getSignInToken";
        private static final String CMD_ENABLE_DEVICE_LOADING = "enableDeviceLoading";
        private static final String CMD_AUTH_REQ_PUSH_TOKEN = "AUTH_REQ_PUSH_TOKEN";
        Context mContext;
        GoogleSignInClient mGoogleSignInClient;
        private String mToken;
        /** Instantiate the interface and set the context */
        WebAppInterface(Context c)
        {
            mContext = c;
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.server_client_id))
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(mContext, gso);
        }
        @JavascriptInterface
        public String onWebViewCommand(String command, final String json)
        {
            BLog.d(TAG, "onWebViewCommand", command, json);
            if (CMD_ENABLE_DEVICE_LOADING.equals(command))
            {
                mLoader.post(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            JSONObject obj = new JSONObject(json);
                            boolean enable = obj.getBoolean("enable");
                            mLoader.setVisibility(enable?View.VISIBLE:View.GONE);
                        }
                        catch (Exception e)
                        {
                            BLog.d(TAG, "onWebViewCommand", e);
                        }
                    }
                });
            }
            else if (CMD_AUTH_REQ_PUSH_TOKEN.equals(command))
            {
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        FirebaseInstanceId.getInstance().getInstanceId()
                                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                        if (!task.isSuccessful()) {
                                            BLog.e(TAG, "onComplete", "skip");
                                            return;
                                        }
                                        // Get new Instance ID token
                                        String token = task.getResult().getToken();
                                        updatePushToken(2, MainActivity.this.getApplicationContext(), token);
                                    }
                                });
                    }
                });
            }
            return null;
        }
        @JavascriptInterface
        public void onWebViewLog(String data)
        {
            BLog.d(TAG, "onWebViewLog", data);
        }
        @JavascriptInterface
        public boolean onClickedSignIn(String userAgent, String provider) {
            BLog.d(TAG, "onClickedSignIn", userAgent, provider);
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
            return true;
        }
        @JavascriptInterface
        public boolean onClickLogout() {
            BLog.d(TAG, "onClickLogout");
            mGoogleSignInClient.signOut();
            return true;
        }
        @JavascriptInterface
        public String getSignInToken()
        {
            return mToken;
        }
        public void setSignInToken(String token)
        {
            mToken = token;
        }
        ////////////////////////////////////////////////////////////////////////
        // dispatch
        //

    }

    private static class Client extends WebViewClient {
        // private static final String TAG = "Client";
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            BLog.d(TAG, "shouldOverrideUrlLoading", url);
            view.loadUrl(url);
            return false;
        }
        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error)
        {
            BLog.d(TAG, "onReceivedSslError");
            final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            builder.setMessage("Invalid ssl certificate, wish to proceed?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }
        @Override
        public void onReceivedError(WebView view, int errorCode, String desc, String url)
        {
            BLog.d(TAG, "onReceivedError", errorCode, desc, url);
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            BLog.d(TAG, "onPageStarted", url);
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            BLog.d(TAG, "onPageStarted", url);
        }
    }
    private static class ChromeClient extends WebChromeClient {
        // private static final String TAG = "ChromeClient";
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            //return super.onJsAlert(view, url, message, result);
            BLog.d(TAG, "onJsAlert", message);
            return true;
        }
    }
    ///////////////////////////////////
    //
    //
    public void updatePushToken(int src, Context context, String token)
    {
        if (null==mWebView) return;
        BLog.d(TAG, "updatePushToken", src, token);

        JSONObject json = new JSONObject();
        try {
            json.put("deviceUid", getUID(context));
            json.put("pushToken", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new JSCommandDispatcher(
                mWebView,
                JSCommandDispatcher.CMD_ON_DEVICE_PUSH_TOKEN_UPDATE,
                json,
                new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        BLog.d(TAG, "onReceiveValue", value);
                    }
                }
        );
    }
    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    public synchronized static String getUID(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.apply();
            }
        }
        return uniqueID;
    }
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String pushToken = intent.getStringExtra("pushToken");
            BLog.d(TAG, "onReceive", pushToken);
            updatePushToken(1, MainActivity.this.getApplicationContext(), pushToken);
        }
    };
}
