package app.bettersense.io;

import android.util.Log;

import java.util.Map;

public class BLog
{
    private static final String TAG = BLog.class.getSimpleName();
    private static final boolean ENABLE_LOG = true;
    private static final String TAG_NULL = "null";
    private static String formatLogPrefix(String clsName, String funcName)
    {
       return String.format("[%s][%s]:", clsName, funcName);
    }
    private static String formatLogMsg(Object... msgObj)
    {
        if ( null==msgObj ) return TAG_NULL;
        StringBuilder sb = new StringBuilder();
        for(Object obj : msgObj)
        {
            if ( null==obj ) sb.append(TAG_NULL);
            else sb.append(obj.toString());
            sb.append(" : ");
        }
        return sb.toString();
    }
    public static void stackTrace(String clsName, String funcName)
    {
        StringBuilder builder = new StringBuilder();
        for (StackTraceElement ste : Thread.currentThread().getStackTrace())
        {
            builder.append(ste.toString());
            builder.append(" >>> ");
        }

        String log = String.format("%s %s : %s",
                formatLogPrefix(clsName, funcName),
                "trace", builder.toString());
        Log.d(TAG, log);
    }
    public static String formatShortMsg(String msg, int limit)
    {
        if ( null==msg ) return TAG_NULL;
        return ( limit<msg.length() )?msg.substring(0, limit):msg;
    }
    public static void dPerf(String clsName, String funcName, Object... msgObj)
    {
        if ( false==ENABLE_LOG ) return;

        String log = String.format("%s %s %s",
                "[dPerf]",
                formatLogPrefix(clsName, funcName),
                formatLogMsg(msgObj));
        Log.d(TAG, log);
    }
    public static void d(String clsName, String funcName, Object... msgObj)
    {
        if ( false==ENABLE_LOG ) return;

        String log = String.format("%s %s",
                                    formatLogPrefix(clsName, funcName),
                                    formatLogMsg(msgObj));
        Log.d(TAG, log);
    }
    public static void d(String clsName, String funcName, String msg, Map<String, String> data)
    {
        if ( false==ENABLE_LOG ) return;

        StringBuilder builder = new StringBuilder();
        for (Map.Entry e : data.entrySet())
        {
            builder.append(e.getKey());
            builder.append(": ");
            builder.append(e.getValue());
        }
        String log = String.format("%s %s : %s",
                                    formatLogPrefix(clsName, funcName),
                                    msg, builder.toString());
        Log.d(TAG, log);
    }
    public static void e(String clsName, String funcName, Object... msgObj)
    {
        if ( false==ENABLE_LOG ) return;

        String log = String.format("%s %s",
                                    formatLogPrefix(clsName, funcName),
                                    formatLogMsg(msgObj));
        Log.e(TAG, log);
    }
}
